// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCwg8tF_9McwWMHK3S9qhnZFoCruMJWJgg",
    authDomain: "hello-9ba81.firebaseapp.com",
    databaseURL: "https://hello-9ba81.firebaseio.com",
    projectId: "hello-9ba81",
    storageBucket: "hello-9ba81.appspot.com",
    messagingSenderId: "693313510583",
    appId: "1:693313510583:web:1e1ae1016f8d3b532f4132"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
