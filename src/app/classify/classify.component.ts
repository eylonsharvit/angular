import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
 
  selectedNetwork:string;
  invalid:string;
  show: boolean = false;
  networks: string[] = ['BBC', 'CNN', 'NBC'];
  constructor(private route:ActivatedRoute) { }
 
   ngOnInit(): void {
    this.selectedNetwork = this.route.snapshot.params.network;
      
    if (this.selectedNetwork.toUpperCase()!='BBC' && this.selectedNetwork.toUpperCase()!='CNN' && this.selectedNetwork.toUpperCase()!='NBC' ) {
            this.show = true
            this.invalid=this.selectedNetwork;
          }
          else{
            this.selectedNetwork=this.selectedNetwork.toUpperCase();
          }
    }

  }


